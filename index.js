const path = require('path');
const fs = require('fs');
const deepmerge = require('deepmerge');
const json_format = require('json-format');

// Require webpack internal plugins
const {SingleEntryPlugin, LibraryTemplatePlugin} = require('webpack');

/**
 * Plugin name
 * @type {string}
 */
const PLUGIN_NAME = 'JsnWebpackExtensionManifestPlugin';

/**
 * Plugin child compiler name
 * @type {string}
 */
const PLUGIN_COMPILER_NAME = PLUGIN_NAME + ' Compiler';

/**
 * @typedef JsnWebpackExtensionManifestPlugin~Options
 * @property {string} [name = ''] Name of the plugin instance (this will be used to name intermediate js file)
 * @property {string} manifest Absolute path to the manifest file
 * @property {string} [entryName = 'manifest.json'] Entry name for child compiler
 * @property {string} [outputFilename = 'manifest.json'] Path to the output manifest file relative to output.path of main compiler
 * @property {boolean} [useLoader = true] By default plugin will use file-loader to load all files, but if you already configured loaders for files that will be stated in manifest - you may want to disable this option
 * @property {object|object[]} [merge = {}] Fields that will be merged into manifest. Make sure that you DO NOT use prefixed with @ or @! paths to resources, because they WILL NOT be processed. If you want to merge into the manifest more than one object - you can pass to this property array of objects
 */

/**
 * @typedef JsnWebpackExtensionManifestPlugin~ResourceDefinition
 * @property {'none'|'default'|'entry'|'static'} [type = 'none']
 * @property {string} [resource = '']
 * @property {string} req
 */

/**
 * @type {JsnWebpackExtensionManifestPlugin~Options}
 */
const DefaultOptions = {
    name: '',
    entryName: 'manifest.json',
    outputFilename: 'manifest.json',
    intermediateName: '',
    emptyResources: "leave",
    useLoader: true,
    merge: {}
};

const RESOURCE_REGEX = /"(@!?.+?)"/gm;

/**
 * @class JsnWebpackExtensionManifestPlugin
 */
class JsnWebpackExtensionManifestPlugin{

    /**
     * @param {JsnWebpackExtensionManifestPlugin~Options} options
     * @throws Error
     */
    constructor(options = {}){
        options = deepmerge.all([{}, DefaultOptions, options]);

        if(!options.manifest)
            throw new Error('Path to manifest.json should be provided in option \'manifest\'!');

        if(!path.isAbsolute(options.manifest))
            throw new Error('Path to manifest.json should be absolute!');

        /**
         * @type {JsnWebpackExtensionManifestPlugin~Options}
         */
        this.options = options;

        /**
         * @type {string}
         */
        this.manifestPath = path.dirname(this.options.manifest);

        /**
         * @type {string} Name of the plugin instance (used to name intermediate file)
         */
        this.name = this.options.name || this.options.entryName;

        /**
         * @type {string}
         * @private
         */
        this._compilerOutputPath = '';

        /**
         * @type {string} Path to the compiled manifest file
         * @private
         */
        this.__compiledManifestJsPath = '';

        /**
         * @type {boolean}
         * @private
         */
        this.__webpackInWatchMode = false;

        /**
         * @type {boolean}
         * @private
         */
        this.__firstRun = true;

        /**
         * @type {boolean}
         * @private
         */
        this.__manifestInvalidated = false;

        /**
         * @type {boolean}
         * @private
         */
        this.__canBeShutdown = true;

        this._bindMethods();
    }

    _bindMethods(){
        this.watchManifest = this.watchManifest.bind(this);
        this.compileManifest = this.compileManifest.bind(this);
        this.onFileInvalidated = this.onFileInvalidated.bind(this);
        this.emitCompiledManifest = this.emitCompiledManifest.bind(this);

        this._isFileExists = this._isFileExists.bind(this);
        this._getFileContent = this._getFileContent.bind(this);
        this._grabResourceRequests = this._grabResourceRequests.bind(this);
        this._parseResourceRequest = this._parseResourceRequest.bind(this);
        this._createIntermediateJsFile = this._createIntermediateJsFile.bind(this);
        this._transformReqToJsRequire = this._transformReqToJsRequire.bind(this);
        this._writeIntermediateJsContent = this._writeIntermediateJsContent.bind(this);
        this._runIntermediateCompilation = this._runIntermediateCompilation.bind(this);
        this._replaceResourcesInManifest = this._replaceResourcesInManifest.bind(this);
        this._writeResultingManifest = this._writeResultingManifest.bind(this);
        this._lateResolveEntryPoints = this._lateResolveEntryPoints.bind(this);
    }

    /**
     * Returns name for the intermediate file
     * @returns {string}
     */
    get intermediateFilename(){
        return this.name + '.intermediate.js';
    }

    /**
     * @returns {string}
     */
    get intermediateBuiltFilenameTemplate(){
        return this.name + '.intermediate.build.js';
    }

    /**
     * Returns absolute path to the intermediate file
     * @returns {string}
     */
    get intermediatePathname(){
        return path.resolve(__dirname, 'manifest-js', this.intermediateFilename);
    }

    /**
     * Applies plugin to the webpack compiler
     * @param compiler
     */
    apply(compiler){
        this._compilerOutputPath = compiler.options.output.path;

        compiler.hooks.run.tapAsync(PLUGIN_NAME, (compiler, cb) => {this.__webpackInWatchMode = false; cb();});
        compiler.hooks.watchRun.tapAsync(PLUGIN_NAME, (compiler, cb) => {this.__webpackInWatchMode = true; cb();});

        compiler.hooks.emit.tapAsync(PLUGIN_NAME, (compilation, cb) => {this.compileManifest(compilation).then(cb).catch(cb);});
        compiler.hooks.afterCompile.tapAsync(PLUGIN_NAME, this.watchManifest);
        compiler.hooks.invalid.tap(PLUGIN_NAME, this.onFileInvalidated);

        compiler.hooks.afterEmit.tapAsync(PLUGIN_NAME, (compilation, cb) => {this.emitCompiledManifest(compilation).then(cb).catch(cb);});
        //compiler.hooks.done.tapAsync(PLUGIN_NAME, (stats, cb) => {this.__webpackInWatchMode ? cb() : this.emitCompiledManifest(compiler).then(cb).catch(cb)});

        compiler.hooks.done.tapAsync(PLUGIN_NAME, (stats, cb) => {!this.__webpackInWatchMode ? this.shutdown() : null; cb();});
        compiler.hooks.failed.tap(PLUGIN_NAME, err => {!this.__webpackInWatchMode ? this.shutdown() : null;});
        compiler.hooks.watchClose.tap(PLUGIN_NAME, this.shutdown);
    }

    /**
     * Adds manifest.json file to webpack's watch list
     * @param compilation
     * @param {Function} callback
     */
    watchManifest(compilation, callback){
        if(this.__webpackInWatchMode)
            compilation.fileDependencies.add(this.options.manifest);

        callback();
    }

    /**
     * Compiles manifest
     * @param compilation
     * @returns {Promise<void>}
     */
    async compileManifest(compilation){
        if(!this.__firstRun && !this.__manifestInvalidated)
            return;

        if(this.__firstRun)
            this.__firstRun = false;

        if(this.__manifestInvalidated)
            this.__manifestInvalidated = false;

        if(!(await this._isFileExists(this.options.manifest)))
            return;


        // Already built assets
        const assets = compilation.modules
            .filter(({buildInfo}) => buildInfo.assets)
            .reduce((obj, {resource, buildInfo}) => {
                obj[resource] = Object.keys(buildInfo.assets)[0];
                return obj;
            }, {});

        let manifest = await this._getFileContent(this.options.manifest);
        let resRequests = await this._grabResourceRequests(manifest);
        let jsContent = await this._createIntermediateJsFile(resRequests, assets);
        await this._writeIntermediateJsContent(jsContent);
        await this._runIntermediateCompilation(compilation);
    }

    /**
     * @param {string} file
     * @param {Number} [constants = fs.constants.F_OK]
     * @private
     */
    _isFileExists(file, constants = fs.constants.F_OK){
        return new Promise(resolve => fs.access(file, constants, err => resolve(!err)));
    }

    /**
     * @param {string} file
     * @returns {Promise<string>}
     * @private
     */
    _getFileContent(file){
        return new Promise((resolve, reject) => {
            fs.readFile(file, {encoding: 'utf8', flag: 'r'}, (err, data) => {
                if(err) return reject(err);
                resolve(data);
            });
        });
    }

    /**
     * @param {string} content
     * @returns {Set<JsnWebpackExtensionManifestPlugin~ResourceDefinition>}
     * @private
     */
    _grabResourceRequests(content){
        let m, regex = RESOURCE_REGEX, resources = new Set();

        while((m = regex.exec(content)) !== null){
            if(m.index === regex.lastIndex)
                regex.lastIndex++;

            resources.add(this._parseResourceRequest(m[1]));
        }

        return resources;
    }

    /**
     * @param resourceRequest
     * @returns {JsnWebpackExtensionManifestPlugin~ResourceDefinition}
     * @private
     */
    _parseResourceRequest(resourceRequest){
        const o = {type: 'none', resource: '', req: resourceRequest};

        if(resourceRequest.indexOf('@!') === 0){
            o.type = 'entry';
            o.resource = resourceRequest.substr(2);
        }else{
            o.type = 'static';
            o.resource = resourceRequest.substr(1);
        }

        return o;
    }

    /**
     * @param {Set<JsnWebpackExtensionManifestPlugin~ResourceDefinition>} resRequests
     * @param {object} [builtAssets = {}]
     * @returns {string}
     * @private
     */
    async _createIntermediateJsFile(resRequests, builtAssets = []){
        let js = 'module.exports={',
            reqs = [];

        for(const [k, req] of resRequests.entries())
            reqs.push(await this._transformReqToJsRequire(req, builtAssets));

        js += reqs.join(",\n");
        js += '};';

        return js;
    }

    /**
     * @param {JsnWebpackExtensionManifestPlugin~ResourceDefinition} req
     * @param {object} [builtAssets = {}]
     * @returns {string}
     * @private
     */
    async _transformReqToJsRequire(req, builtAssets = {}){
        if(req.type === "static"){
            const resourcePath = path.resolve(this.manifestPath, req.resource);

            if(builtAssets[resourcePath])
                return `"${req.req}":"${path.normalize(builtAssets[resourcePath]).replace(/\\/g, '/')}"`;

            if(!(await this._isFileExists(resourcePath)))
                return `// "${req.req}":"${resourcePath}" // Resource not found`;

            const fileLoader = this.options.useLoader ? 'file-loader!' : '';
            const pathname = fileLoader + path.normalize(resourcePath).replace(/\\/g, '/');
            return `"${req.req}":require('${pathname}')`;
        }else{
            return `"${req.req}":"${req.req}"`;
        }
    }

    /**
     * @param {string} content
     * @returns {Promise<void>}
     * @private
     */
    _writeIntermediateJsContent(content){
        return new Promise((resolve, reject) => {
            fs.writeFile(this.intermediatePathname, content, {encoding: 'utf8', mode: 0o777, flag: 'w'}, err => {
                if(err) return reject(err);

                resolve();
            });
        });
    }

    /**
     * @param compilation
     * @returns {Promise<void>}
     * @private
     */
    _runIntermediateCompilation(compilation){
        const output = {
            filename: this.intermediateBuiltFilenameTemplate,
            path: compilation.compiler.options.output.path
        };

        const childCompiler = compilation.createChildCompiler(PLUGIN_COMPILER_NAME, output);

        new SingleEntryPlugin(compilation.compiler.context, this.intermediatePathname, this.options.entryName).apply(childCompiler);
        //new NodeTemplatePlugin(output).apply(childCompiler);
        //new NodeTargetPlugin().apply(childCompiler);
        //new LoaderTargetPlugin('node').apply(childCompiler);
        new LibraryTemplatePlugin('manifest', 'commonjs2').apply(childCompiler);

        return new Promise((resolve, reject) => {
            this.__canBeShutdown = false;

            childCompiler.runAsChild((err, entries, childCompilation) => {
                if(err){
                    this.__compiledManifestJsPath = '';
                    return reject(err);
                }

                this.__compiledManifestJsPath = path.resolve(childCompilation.compiler.options.output.path, entries[0].files[0]);
                this.__canBeShutdown = true;
                resolve();
            });
        });
    }

    /**
     * @param {string} file
     * @param {Number} changeTime
     */
    onFileInvalidated(file, changeTime){
        if(file === this.options.manifest)
            this.__manifestInvalidated = true;
    }

    /**
     * @param compilation
     * @returns {Promise<void>}
     */
    async emitCompiledManifest(compilation){
        if(this.__compiledManifestJsPath === '' || !(await this._isFileExists(this.__compiledManifestJsPath)))
            throw new Error('Unable to find compiled manifest file');

        // Clear cache
        delete require.cache[require.resolve(this.__compiledManifestJsPath)];
        let list = require(this.__compiledManifestJsPath);

        for(const key of Object.keys(list))
            list[key] = list[key].replace(/\\/g, '/');

        // Resolve entry points
        list = this._lateResolveEntryPoints(compilation, list);

        let manifestContent = await this._getFileContent(this.options.manifest);
        manifestContent = this._replaceResourcesInManifest(manifestContent, list);
        let manifestObj = JSON.parse(manifestContent);

        if(Array.isArray(this.options.merge)){
            manifestObj = deepmerge([manifestObj, ...this.options.merge]);
        }else{
            manifestObj = deepmerge(manifestObj, this.options.merge);
        }

        const manifestJSON = json_format(manifestObj);
        await this._writeResultingManifest(manifestJSON);
    }

    /**
     * @param {string} content
     * @param {object} resources
     * @returns {string}
     * @private
     */
    _replaceResourcesInManifest(content, resources){
        return content.replace(RESOURCE_REGEX, (m, p1) => `"${resources[p1]}"`);
    }

    /**
     * @param {string} json
     * @returns {Promise<void>}
     * @private
     */
    _writeResultingManifest(json){
        return new Promise((resolve, reject) => {
            if(!this._compilerOutputPath)
                return reject();

            const outputManifestPath = path.resolve(this._compilerOutputPath, this.options.outputFilename);

            fs.writeFile(outputManifestPath, json, {encoding: 'utf8', flag: 'w'}, err => {
                if(err) return reject(err);
                resolve();
            });
        });
    }

    /**
     * @param compilation
     * @param {object} list
     * @private
     */
    _lateResolveEntryPoints(compilation, list){
        for(const key of Object.keys(list)){
            const res = list[key];

            if(res.indexOf('@!') !== 0)
                continue;

            list[key] = '';
            const entryName = res.substr(2);

            for(const chunk of compilation.chunks){
                if(chunk.id === entryName || chunk.name === entryName){
                    list[key] = chunk.files[0];
                    break;
                }
            }
        }

        return list;
    }

    /**
     * Removes intermediate files, etc.
     */
    shutdown(){
        if(!this.__canBeShutdown)
            return;

        try{fs.unlinkSync(this.intermediatePathname);}catch(err){}
        if(this.__compiledManifestJsPath)
            try{fs.unlinkSync(this.__compiledManifestJsPath);}catch(err){}
    }

}

module.exports = JsnWebpackExtensionManifestPlugin;
# JsnWebpackManifestExtensionPlugin
#### Should work with webpack >= 4.41.2 and nodejs >= 8.10.0

Plugin simplifies the development of browser extensions

![npm](https://img.shields.io/npm/v/jsn-webpack-extension-manifest-plugin?style=for-the-badge)![npm](https://img.shields.io/npm/dw/jsn-webpack-extension-manifest-plugin?style=for-the-badge)![npm](https://img.shields.io/npm/dt/jsn-webpack-extension-manifest-plugin?style=for-the-badge)
![node](https://img.shields.io/node/v/jsn-webpack-extension-manifest-plugin?style=for-the-badge)

## Install
```bash
npm i -D jsn-webpack-extension-manifest-plugin
```

## Description and usage

This plugin allows to use in chrome extension's manifest.json file paths to resources with prefixes
@ and @!.

Prefix **@** will be resolved during webpack compilation to resource path loaded with file-loader.

Prefix **@!** will be resolved during webpack compilation to compiled chunk path.

### Examples

Project structure:

- **build**
- **src**
    - **client**
        - client-script.js
    - hello_world.js
- manifest.json
        
Webpack config:
```js
module.exports = {
    entry: {
        helloworld: 'src/hello_world.js'
    },
    output: {
        filename: 'js/[name].build.js',
        path: path.resolve(__dirname, 'build')
    },
    module: {
        rules: [
            {test: /src\/client\/.+\.js$/, use: 'file-loader'}
        ]
    },
    plugins: [
        new JsnWebpackExtensionManifestPlugin({
            manifest: path.resolve(__dirname, 'manifest.json'),
            outputFilename: 'manifest.json',
            useLoader: false
        })
    ]
};
```

Manifest:
```json
{
  "background": [
    "@!helloworld"
  ],
  "web_accessible_resources": [
    "@src/client/client-script.js"
  ]
}
```

After compilation manifest will be placed to **build** directory and resources prefixed with @ or @!
will be replaced.

Result manifest.json
```json
{
  "background": [
    "js/helloworld.build.js"
  ],
  "web_accessible_resources": [
    "js/client/c265935b2ca8ab17df005645b08b714c.js"
  ]
}
```

## Plugin options
| Option | Type | Allowed values | Default value | Description |
|--------|------|----------------|---------------|-------------|
| name (optional) |string| * | | Name of the plugin instance (this will be used to name intermediate js file)|
| manifest (required) | string | * | | Absolute path to the manifest file
| entryName (optional) | string | * | manifest.json | Entry name for child compiler |
| outputFilename (optional) | string | * | manifest.json | Path to the output manifest file relative to output.path of main compiler |
| useLoader (optional) | boolean | true, false | true | By default plugin will use file-loader to load all files, but if you already configured loaders for files that will be stated in manifest - you may want to disable this option |
| merge (optional) | object, object[] | * | {} | Fields that will be merged into manifest. Make sure that you DO NOT use prefixed with @ or @! paths to resources, because they WILL NOT be processed. If you want to merge into the manifest more than one object - you can pass to this property array of objects |


